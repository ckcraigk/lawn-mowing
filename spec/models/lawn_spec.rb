require "rails_helper"

describe Lawn, '.build_layout' do
  it 'builds layout given dimensions' do
    lawn = Lawn.new(width: 5, height: 4)
    lawn.build_layout

    expect(lawn.layout.length-1).to eq lawn.width

    lawn.layout.each do |column|
      expect(column.length-1).to eq lawn.height
    end
  end
end

describe Lawn, '.add_mowers_to_layout' do
  it 'adds mowers to lawn layout at mower coordinates' do
    lawn = Lawn.new(width: 5, height: 5)
    lawn.build_layout
    mower1 = Mower.new(x: 1, y: 2, heading: "N", commands: "LMLMLMLMM")
    mower2 = Mower.new(x: 3, y: 3, heading: "E", commands: "MMRMMRMRRM")
    lawn.mowers << mower1
    lawn.mowers << mower2
    
    lawn.add_mowers_to_layout

    result = lawn.layout
    
    expect(result[mower1.x][mower1.y]).to eq mower1
    expect(result[mower2.x][mower2.y]).to eq mower2
  end
end

describe Lawn, '.mow' do
  it 'mows lawn for all mowers' do
    lawn = Lawn.new(width: 5, height: 5)
    lawn.build_layout

    mower1 = Mower.new(x: 1, y: 2, heading: "N", commands: "LMLMLMLMM")
    mower2 = Mower.new(x: 3, y: 3, heading: "E", commands: "MMRMMRMRRM")
    lawn.mowers << mower1
    lawn.mowers << mower2
    lawn.add_mowers_to_layout
    
    lawn.mow

    result = lawn.layout

    expect(result[1][3]).to eq mower1
    expect(result[5][1]).to eq mower2
  end
end

describe Lawn, '.traverse_lawn' do
  it 'traverses lawn for individual mower' do
    lawn = Lawn.new(width: 5, height: 5)
    lawn.build_layout
    mower = Mower.new(x: 1, y: 2, heading: "N", commands: "LMLMLMLMM")
    lawn.mowers << mower
    lawn.add_mowers_to_layout
    
    lawn.traverse_lawn(mower)

    result = lawn.layout

    expect(mower.x).to eq 1
    expect(mower.y).to eq 3
    expect(mower.heading).to eq "N"
    expect(result[1][3]).to eq mower
  end
end

describe Lawn, '.is_available?' do
  it 'checks layout for available spaces to prevent collision' do
    lawn = Lawn.new(width: 5, height: 5)
    lawn.build_layout
    mower = Mower.new(x: 1, y: 2, heading: "N")
    lawn.mowers << mower
    lawn.add_mowers_to_layout

    result = lawn.is_available?(1, 2)

    expect(result).to eq false
  end
end