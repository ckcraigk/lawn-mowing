require "rails_helper"

describe Mower, '.update_coordinates' do
  it 'updates x and y coordinates' do
    mower = Mower.new(x: 5, y: 4)
    mower.update_coordinates(3, 2)

    expect(mower.x).to eq 3
    expect(mower.y).to eq 2
  end
end

describe Mower, '.change_direction' do
  it 'changes direction of heading given pivot L or R' do
    mower = Mower.new(x: 1, y: 2, heading: "N", commands: "LMLMLMLMM")
    mower.change_direction("L")
    
    expect(mower.heading).to eq "W"
   
    mower.change_direction("R")
    
    expect(mower.heading).to eq "N"
  end
end

describe Mower, '.position' do
  it 'describes position of mower' do
    mower = Mower.new(x: 1, y: 2, heading: "N", commands: "LMLMLMLMM")
    
    result = mower.position

    expect(result).to eq "1 2 N"
  end
end