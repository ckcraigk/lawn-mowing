class Mower < ActiveRecord::Base
  belongs_to :lawn

  def update_coordinates(x, y)
    self.x = x
    self.y = y
  end

  def change_direction(direction)
    if direction == "L"
      case heading
      when "N"
        self.heading = "W"
      when "W"
        self.heading = "S"
      when "S"
        self.heading = "E"
      when "E"
        self.heading = "N"
      end
    elsif direction == "R"
      case heading
      when "N"
        self.heading = "E"
      when "W"
        self.heading = "N"
      when "S"
        self.heading = "W"
      when "E"
        self.heading = "S"
      end  
    end
  end

  def position
    self.x.to_s + " " + self.y.to_s + " " + self.heading
  end

end
