class Lawn < ActiveRecord::Base
  has_many :mowers

  attr_accessor :layout

  def build_layout
    @layout = Array.new(width+1, nil).map{ |row| Array.new(height+1, nil) }
  end

  def add_mowers_to_layout
    mowers.each do |mower|
      @layout[mower.x][mower.y] = mower if is_available?(mower.x, mower.y)
    end
  end

  def mow
    mowers.each do |mower|
      traverse_lawn(mower)
    end
  end

  def traverse_lawn(mower)
    @x = mower.x
    @y = mower.y

    mower.commands.split("").each do |command|
      if command == 'L' || command == 'R'
        mower.change_direction(command)
      elsif command == 'M'
        @x, @y = new_coordinates(@x, @y, mower.heading)
        move_mower(@x, @y, mower)
      end
    end
  end

  def is_available?(x, y)
    within_lawn?(x, y) && empty_space?(x, y)
  end

  private

  def within_lawn?(x, y)
    return false unless x >= 0 && y >= 0

    if (x <= width) && (y <= height)
      return true
    else
      return false
    end
  end

  def empty_space?(x, y)
    @layout[x][y].nil?
  end

  def new_coordinates(x, y, heading)
    case heading
    when 'N'
      return x, y+1
    when 'S'
      return x, y-1
    when 'E'
      return x+1, y
    when 'W'
      return x-1, y
    end
  end

  def move_mower(x, y, mower)
    if is_available?(x, y)
      update_position(x, y, mower)
      mower.update_coordinates(x, y)
    else
      "Cannot move mower to coordinates: " + x.to_s + "," + y.to_s
    end
  end

  def update_position(x, y, mower)
    @layout[mower.x][mower.y] = nil
    @layout[x][y] = mower
  end

end