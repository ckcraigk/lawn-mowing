#!/bin/env ruby

require_relative "../config/environment.rb"

input = $stdin.readlines

lawn_coordinates = input.delete_at(0).chomp.split(" ")
@lawn = Lawn.new(width: lawn_coordinates[0].to_i, height: lawn_coordinates[1].to_i)

input.each_slice(2) do |mower|
  mower_position = mower[0].chomp.split(" ")
  x = mower_position[0]
  y = mower_position[1]
  heading = mower_position[2]
  commands = mower[1].chomp!

  @lawn.mowers << Mower.new(x: x, y: y, heading: heading, commands: commands)
end

@lawn.build_layout
@lawn.add_mowers_to_layout
@lawn.mow

puts @lawn.mowers.map{ |mower| mower.position }