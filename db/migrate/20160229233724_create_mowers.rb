class CreateMowers < ActiveRecord::Migration
  def change
    create_table :mowers do |t|
      t.integer :x
      t.integer :y
      t.string :heading
      t.string :commands
      t.references :lawn, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
