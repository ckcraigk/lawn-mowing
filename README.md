# README #

Application to demo mowing lawn for N mowers across given Lawn size.

### Setup ###

Lawns and mowers can be set up and simulated using lib/part_one.rb, this takes user input as params through the console.

To run use "ruby part_one.rb", this will read data until EOF, to declare EOF use Ctrl+D on Unix and Ctrl+Z on Windows.

### Testing ###

Lawn and Mower configurations are all tested using RSpec-rails, to run tests simply rspec spec/models.

### Who do I talk to? ###

* @ckcraigk